namespace Дурака.Основа.Модели.Карты;

public class ИгральнаяКарта : Карта
{
    public МастьКарты Масть { get; set; }
    public ЗначениеКарты Значение { get; set; }

    public override string КороткоеОписание()
    {
        return $"{Значение} {Масть}";
    }
}