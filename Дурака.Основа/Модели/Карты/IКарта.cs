namespace Дурака.Основа.Модели.Карты;

public interface IКарта
{
    public Guid Id { get; set; }
    public string КороткоеОписание();
}