namespace Дурака.Основа.Модели.Карты;

public abstract class Карта : IКарта
{
    public Guid Id { get; set; } = Guid.NewGuid();
    public abstract string КороткоеОписание();

    public sealed override string ToString()
    {
        return КороткоеОписание();
    }
}