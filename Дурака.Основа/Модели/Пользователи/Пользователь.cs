namespace Дурака.Основа.Модели.Пользователи;

public class Пользователь
{
    public Guid Id { get; set; }
    public string Соль { get; set; } 
    public string СоленыйХэшПароля { get; set; }
    public string Email { get; set; }
    public string Логин { get; set; }

    public Пользователь(string соль, string соленыйХэшПароля, string email, string логин)
    {
        Соль = соль;
        СоленыйХэшПароля = соленыйХэшПароля;
        Email = email;
        Логин = логин;
    }
}