﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Дурака.ИгровойСервер.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ИгровоеПоложениеИгрыДурака",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ИгровоеПоложениеИгрыДурака", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Пользователи",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Соль = table.Column<string>(type: "text", nullable: false),
                    СоленыйХэшПароля = table.Column<string>(type: "text", nullable: false),
                    Email = table.Column<string>(type: "text", nullable: false),
                    Логин = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Пользователи", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ИгральнаяКарта",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Масть = table.Column<int>(type: "integer", nullable: false),
                    Значение = table.Column<int>(type: "integer", nullable: false),
                    ИгровоеПоложениеИгрыДуракаId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ИгральнаяКарта", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ИгральнаяКарта_ИгровоеПоложениеИгрыДурака_ИгровоеПоложениеИ~",
                        column: x => x.ИгровоеПоложениеИгрыДуракаId,
                        principalTable: "ИгровоеПоложениеИгрыДурака",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Игры",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    ИгровоеПоложениеId = table.Column<Guid>(type: "uuid", nullable: true),
                    Состояние = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Игры", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Игры_ИгровоеПоложениеИгрыДурака_ИгровоеПоложениеId",
                        column: x => x.ИгровоеПоложениеId,
                        principalTable: "ИгровоеПоложениеИгрыДурака",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_ИгральнаяКарта_ИгровоеПоложениеИгрыДуракаId",
                table: "ИгральнаяКарта",
                column: "ИгровоеПоложениеИгрыДуракаId");

            migrationBuilder.CreateIndex(
                name: "IX_Игры_ИгровоеПоложениеId",
                table: "Игры",
                column: "ИгровоеПоложениеId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ИгральнаяКарта");

            migrationBuilder.DropTable(
                name: "Игры");

            migrationBuilder.DropTable(
                name: "Пользователи");

            migrationBuilder.DropTable(
                name: "ИгровоеПоложениеИгрыДурака");
        }
    }
}
