using CSharpFunctionalExtensions;

namespace Дурака.ИгровойСервер.Контроллеры.Auth;

public class AuthErrorResponse : AuthResponse
{
    private Result Результат { get; }
    public override string Type => "ERROR";
    public override object Response => Результат.Error;

    public AuthErrorResponse(Result результат)
    {
        Результат = результат;
    }
}