namespace Дурака.ИгровойСервер.Контроллеры.Auth;

public class SignUpResponse : AuthResponse
{
    public override string Type => "OK";
    public override object Response => "Регистрация успешно завершена!";
}