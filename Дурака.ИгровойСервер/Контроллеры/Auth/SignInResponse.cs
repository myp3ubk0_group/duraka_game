using Дурака.Основа.Модели.Пользователи;

namespace Дурака.ИгровойСервер.Контроллеры.Auth;

public class SignInResponse : AuthResponse
{
    private Пользователь Пользователь { get; }
    public override string Type => "OK";
    public override object Response => Пользователь;

    public SignInResponse(Пользователь пользователь)
    {
        Пользователь = пользователь;
    }
}