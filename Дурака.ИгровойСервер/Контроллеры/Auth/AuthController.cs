using CSharpFunctionalExtensions;
using Microsoft.AspNetCore.Mvc;
using Дурака.ИгровойСервер.Сервисы;
using Дурака.Основа.Модели.Пользователи;

namespace Дурака.ИгровойСервер.Контроллеры.Auth;

[ApiController]
[Route("api/[controller]")]
public class AuthController : ControllerBase
{
    private readonly ILogger<AuthController> _logger;
    private readonly IСервисПользователей _сервисПользователей;

    public AuthController(IСервисПользователей сервисПользователей, ILogger<AuthController> logger)
    {
        _сервисПользователей = сервисПользователей;
        _logger = logger;
    }
    
    [HttpGet("SignIn")]
    public AuthResponse SignIn(string? loginOrEmail, string? password)
    {
        Result<Пользователь> результат = _сервисПользователей.Авторизовать(loginOrEmail, password);
        if (результат.IsFailure)
        {
            return new AuthErrorResponse(результат);
        }

        return new SignInResponse(результат.Value);
    }
    
    [HttpGet("SignUp")]
    public AuthResponse SignUp(string? login, string? email, string? password)
    {
        Result результат = _сервисПользователей.СоздатьНового(login, email, password);
        if (результат.IsFailure)
        {
            return new AuthErrorResponse(результат);
        }

        return new SignUpResponse();
    }
}