namespace Дурака.ИгровойСервер.Контроллеры.Auth;

public abstract class AuthResponse
{
    public abstract string Type { get; }
    public abstract object Response { get; }
}