using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using CSharpFunctionalExtensions;
using Дурака.Основа.Модели.Пользователи;

namespace Дурака.ИгровойСервер.Сервисы;

public class СервисПользователей : IСервисПользователей
{
    private List<Пользователь> _пользователи { get; set; } = new List<Пользователь>();
    public IEnumerable<Пользователь> Пользователи => _пользователи;

    public Result СоздатьНового(string? логин, string? email, string? пароль)
    {
        bool логинКорректен = логин != null && Regex.IsMatch(логин, "^[a-zA-Zа-яА-Я0-9]{4,16}$");
        bool emailКорректен = email != null && MailAddress.TryCreate(email, out _);
        bool парольКорректен = пароль != null && Regex.IsMatch(пароль, "^[a-zA-Zа-яА-Я0-9]{4,16}$");
        if (!логинКорректен || !emailКорректен || !парольКорректен)
        {
            List<string> списокОшибок = new List<string?>()
            {
                логинКорректен ? null : "логин",
                emailКорректен ? null : "email",
                парольКорректен ? null : "пароль"
            }.Where(x => x != null).OfType<string>().ToList();
            return Result.Failure($"Не корректны следующие поля: {string.Join(", ", списокОшибок)}");
        }

        Result<Пользователь> резульатПоискаПоЛогину = ПолучитьПоЛогинуИлиEmail(логин);
        Result<Пользователь> резульатПоискаПоEmail = ПолучитьПоЛогинуИлиEmail(email);
        if (резульатПоискаПоЛогину.IsSuccess || резульатПоискаПоEmail.IsSuccess)
        {
            return Result.Failure("Пользователь уже существует");
        }

        string соль = Guid.NewGuid().ToString("N");
        Пользователь новыйПользователь = new Пользователь(соль, 
            СгенерироватьХэш(ПолучитьСоленуюСтроку(соль, пароль)), email, логин);
        новыйПользователь.Id = Guid.NewGuid();
        _пользователи.Add(новыйПользователь);
        return Result.Success();
    }

    public Result<Пользователь> ПолучитьПоИд(Guid id)
    {
        Пользователь? найденныйПользователь = _пользователи.SingleOrDefault(x => x.Id == id);
        if (найденныйПользователь == null)
        {
            return Result.Failure<Пользователь>("Не найден пользователь");
        }

        return найденныйПользователь;
    }

    public Result<Пользователь> Авторизовать(string? логинИлиEmail, string? пароль)
    {
        if (логинИлиEmail == null || string.IsNullOrEmpty(логинИлиEmail))
        {
            return Result.Failure<Пользователь>("Не указан логин или email");
        }

        if (пароль == null)
        {
            return Result.Failure<Пользователь>("Не указан пароль");
        }
        
        
        Result<Пользователь> результатПолученияПользователя = ПолучитьПоЛогинуИлиEmail(логинИлиEmail);
        if (результатПолученияПользователя.IsFailure)
        {
            return результатПолученияПользователя;
        }

        Пользователь пользователь = результатПолученияПользователя.Value;
        string перец = Guid.NewGuid().ToString("N");
        if (СовпадаетЛиПароль(пользователь, пароль, перец))
        {
            return пользователь;
        }

        return Result.Failure<Пользователь>("Пароль не совпадает");
    }

    private Result<Пользователь> ПолучитьПоЛогинуИлиEmail(string логинИлиEmail)
    {
        List<Пользователь> найденныеПользователи = 
            _пользователи.Where(x => x.Логин == логинИлиEmail || x.Email == логинИлиEmail).ToList();
        switch (найденныеПользователи.Count)
        {
            case (<= 0):
                return Result.Failure<Пользователь>("Не найден пользователь");
            case (1):
                return найденныеПользователи.Single();
            case (>1):
                return Result.Failure<Пользователь>("Не удалось определить пользователя однозначно");
        }
    }

    private bool СовпадаетЛиПароль(Пользователь пользователь, string пароль, string перец)
    {
        string перченыйСоленыйХэшПароляПользователя = СгенерироватьХэш(ПолучитьСоленуюСтроку(перец, пользователь.СоленыйХэшПароля));
        string соленыйХэшПолученногоПароля = СгенерироватьХэш(ПолучитьСоленуюСтроку(пользователь.Соль, пароль));
        string перченыйСоленыйХэшПолученногоПароля = СгенерироватьХэш(ПолучитьСоленуюСтроку(перец, соленыйХэшПолученногоПароля));
        return перченыйСоленыйХэшПароляПользователя == перченыйСоленыйХэшПолученногоПароля;
    }
    
    private string ПолучитьСоленуюСтроку(string соль, string строка)
    {
        return соль + строка;
    }

    private string СгенерироватьХэш(string строка)
    {
        byte[] bytes = MD5.HashData(Encoding.UTF8.GetBytes(строка));
        return Encoding.UTF8.GetString(bytes);
    }
}