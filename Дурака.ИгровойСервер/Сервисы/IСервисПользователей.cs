using CSharpFunctionalExtensions;
using Дурака.Основа.Модели.Пользователи;

namespace Дурака.ИгровойСервер.Сервисы;

public interface IСервисПользователей
{
    IEnumerable<Пользователь> Пользователи { get; }
    Result СоздатьНового(string? логин, string? email, string? пароль);
    Result<Пользователь> ПолучитьПоИд(Guid id);
    Result<Пользователь> Авторизовать(string? логинИлиEmail, string? пароль);
}