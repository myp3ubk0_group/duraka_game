using Microsoft.EntityFrameworkCore;
using Дурака.ИгровойСервер.Модели;
using Дурака.Основа.Модели.Пользователи;

namespace Дурака.ИгровойСервер.Сервисы;

public class ApplicationDbContext : DbContext
{
    public DbSet<Пользователь> Пользователи { get; set; }
    public DbSet<Игра> Игры { get; set; }
    
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
        Database.EnsureCreated();   // создаем базу данных при первом обращении
    }
}