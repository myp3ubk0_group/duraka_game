using CSharpFunctionalExtensions;
using Дурака.Основа.Модели.Карты;

namespace Дурака.ИгровойСервер.Модели;

public interface IИгровоеПоложение<TКарта> 
    where TКарта: IКарта, new()
{
    public Result Инициализировать();
    public Result ПеренестиКартуИзКолодыВРукуКрасногоИгрока();
    public Result ПеренестиКартуИзКолодыВРукуСинегоИгрока();
    public Result<Guid?> ПолучитьКартуИзКолоды();
    public Result<List<ПараКарт>> ПолучитьТекущееПоле();
    public Result<int> ПолучитьНомерХода();
}