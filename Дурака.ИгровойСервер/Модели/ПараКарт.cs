namespace Дурака.ИгровойСервер.Модели;

public class ПараКарт
{
    public Guid ИдБьющейКарты { get; set; }
    public Guid? ИдОтбивающейКарты { get; set; }

    public ПараКарт(Guid идБьющейКарты)
    {
        ИдБьющейКарты = идБьющейКарты;
    }

    public override string ToString()
    {
        return $"Бьющая - {ИдБьющейКарты}, отбивающая - {ИдОтбивающейКарты?.ToString() ?? "<отсутствует>"}";
    }
}