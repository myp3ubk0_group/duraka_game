namespace Дурака.ИгровойСервер.Модели;

public enum СостояниеИгры
{
    НеНачата = 0,
    ВПроцессе = 1,
    Завершена = 2
}