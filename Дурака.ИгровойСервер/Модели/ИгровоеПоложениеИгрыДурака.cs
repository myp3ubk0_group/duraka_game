using CSharpFunctionalExtensions;
using Дурака.Основа.Модели.Карты;

namespace Дурака.ИгровойСервер.Модели;

public class ИгровоеПоложениеИгрыДурака : IИгровоеПоложение<ИгральнаяКарта>
{
    public Guid Id { get; set; } = new Guid();
    private int НомерХода { get; set; } = 0;
    public List<ИгральнаяКарта>? СписокКарт { get; set; }
    private List<Guid>? Колода { get; set; }
    private List<Guid>? Бита { get; set; }
    private List<ПараКарт>? Поле { get; set; }
    private List<Guid>? РукаКрасногоИгрока { get; set; }
    private List<Guid>? РукаСинегоИгрока { get; set; }

    private Result<ИгральнаяКарта> ПолучитьКартуИзСписка(Guid идентификатор)
    {
        if (СписокКарт == null)
        {
            return Result.Failure<ИгральнаяКарта>("Список карт не был ранее инициализирован!");
        }
        
        ИгральнаяКарта? найденнаяКарта = СписокКарт.FirstOrDefault(карта => карта.Id == идентификатор);
        return найденнаяКарта ?? Result.Failure<ИгральнаяКарта>($"В списке карт нет карты с идентификатором {идентификатор}");
    }
    
    public Result Инициализировать()
    {
        Result результатИнициализацииСпискаКарт = ИнициализироватьСписокКарт();
        if (результатИнициализацииСпискаКарт.IsFailure)
        {
            return результатИнициализацииСпискаКарт;
        }
        
        Result результатИнициализацииКолоды = ИнициализироватьКолоду();
        if (результатИнициализацииКолоды.IsFailure)
        {
            return результатИнициализацииКолоды;
        }
        
        Result результатПеремешиванияКолоды = ПеремешатьКолоду();
        if (результатПеремешиванияКолоды.IsFailure)
        {
            return результатПеремешиванияКолоды;
        }
        
        Бита = new List<Guid>();
        Поле = new List<ПараКарт>();
        РукаКрасногоИгрока = new List<Guid>();
        РукаСинегоИгрока = new List<Guid>();
        РаздатьКартыИгрокам();
        return Result.Success();
    }

    private Result РаздатьКартыИгрокам()
    {
        for (int счетчик = 0; счетчик < 6; счетчик++)
        {
            ПеренестиКартуИзКолодыВРукуКрасногоИгрока();
            ПеренестиКартуИзКолодыВРукуСинегоИгрока();
        }
        return Result.Success();
    }

    public Result ПеренестиКартуИзКолодыВРукуКрасногоИгрока()
    {
        if (Колода == null)
        {
            return Result.Failure("Колода не была ранее инициализирована!");
        }

        if (РукаКрасногоИгрока == null)
        {
            return Result.Failure("Рука красного игрока не была ранее инициализирована!");
        }

        Result<Guid?> результатПолученияКартыИзКолоды = ПолучитьКартуИзКолоды();
        if (результатПолученияКартыИзКолоды.IsFailure)
        {
            return Result.Failure("Не удалось перенести карту из колоды в руку красного игрока! " +
                                  $"{результатПолученияКартыИзКолоды.Error}");
        }

        if (результатПолученияКартыИзКолоды.Value != null)
        {
            РукаКрасногоИгрока.Add(результатПолученияКартыИзКолоды.Value.Value);
        }
        return Result.Success();
    }
    
    public Result ПеренестиКартуИзКолодыВРукуСинегоИгрока()
    {
        if (Колода == null)
        {
            return Result.Failure("Колода не была ранее инициализирована!");
        }

        if (РукаСинегоИгрока == null)
        {
            return Result.Failure("Рука синего игрока не была ранее инициализирована!");
        }

        Result<Guid?> результатПолученияКартыИзКолоды = ПолучитьКартуИзКолоды();
        if (результатПолученияКартыИзКолоды.IsFailure)
        {
            return Result.Failure("Не удалось перенести карту из колоды в руку синего игрока! " +
                                  $"{результатПолученияКартыИзКолоды.Error}");
        }

        if (результатПолученияКартыИзКолоды.Value != null)
        {
            РукаСинегоИгрока.Add(результатПолученияКартыИзКолоды.Value.Value);
        }
        return Result.Success();
    }

    public Result<Guid?> ПолучитьКартуИзКолоды()
    {
        if (Колода == null)
        {
            return Result.Failure<Guid?>("Колода не была ранее инициализирована!");
        }

        Guid? верхняяКартаКолоды = Колода.LastOrDefault();
        if (верхняяКартаКолоды == null)
        {
            return null;
        }
        
        Колода.Remove(верхняяКартаКолоды.Value);
        return верхняяКартаКолоды;
    }

    public Result<List<ПараКарт>> ПолучитьТекущееПоле()
    {
        return Поле ?? Result.Failure<List<ПараКарт>>("Поле не было ранее инициализировано!");
    }

    public Result<int> ПолучитьНомерХода()
    {
        return НомерХода;
    }

    private Result ИнициализироватьСписокКарт()
    {
        if (СписокКарт != null)
        {
            return Result.Failure("Список карт был ранее инициализирован!");
        }

        СписокКарт = new List<ИгральнаяКарта>();
        foreach (МастьКарты масть in Enum.GetValues<МастьКарты>())
        {
            foreach (ЗначениеКарты значение in Enum.GetValues<ЗначениеКарты>())
            {
                ИгральнаяКарта новаяКарта = new ИгральнаяКарта();
                новаяКарта.Id = Guid.NewGuid();
                новаяКарта.Масть = масть;
                новаяКарта.Значение = значение;
                СписокКарт.Add(новаяКарта);
            }
        }
        return Result.Success();
    }

    public Result ИнициализироватьКолоду()
    {
        if (Колода == null)
        {
            return Result.Failure("Колода не была ранее инициализирована!");
        }
        
        if (СписокКарт == null)
        {
            return Result.Failure("Список карт не был ранее инициализирован!");
        }

        Колода = СписокКарт.Select(карта => карта.Id).ToList();
        return Result.Success();
    }

    public Result ПеремешатьКолоду()
    {
        if (Колода == null)
        {
            return Result.Failure("Колода не была ранее инициализирована!");
        }

        int количествоКартВКолоде = Колода.Count;
        if (количествоКартВКолоде == 0)
        {
            return Result.Success();
        }

        List<int> картаПеремещений = ПолучитьСписокЧиселДляПеремещенияПоКолоде(Колода.Count);
        List<Guid> новаяКолода = new List<Guid>();
        foreach (int число in картаПеремещений)
        {
            новаяКолода.Add(Колода[число]);
        }
        Колода = новаяКолода;
        return Result.Success();
    }

    private List<int> ПолучитьСписокЧиселДляПеремещенияПоКолоде(int количествоЧисел)
    {
        List<int> числа = Enumerable.Range(0, количествоЧисел).ToList();
        // Перемешивать хотя бы трижды
        for (int количествоПовторений = 0; количествоПовторений < 3; количествоПовторений++)
        {
            int осталосьЧисел = количествоЧисел, случайноеЧисло, перемещаемоеЧисло;
            while (осталосьЧисел > 0)
            {
                случайноеЧисло = Random.Shared.Next(0, осталосьЧисел);
                осталосьЧисел--;
                перемещаемоеЧисло = числа[осталосьЧисел];
                числа[осталосьЧисел] = числа[случайноеЧисло];
                числа[случайноеЧисло] = перемещаемоеЧисло;
            }
        }
        return числа;
    }
}
