namespace Дурака.ИгровойСервер.Модели;

public class СторонаИгры
{
    public Guid Id { get; set; }
    public Guid GameId { get; set; }
    public Guid PlayerId { get; set; }
    public bool Сдался { get; set; } = false;
    public bool СогласенНаНичью { get; set; } = false;

    public СторонаИгры(Guid gameId, Guid playerId)
    {
        GameId = gameId;
        PlayerId = playerId;
    }
}